public with sharing class RevenueReportController {
    @AuraEnabled(cacheable=true)
    public static List<Integer> getAvailableYears() {
        List<Work_Period__c> workPeriods = [
                SELECT Start_Date__c, End_Date__c
                FROM Work_Period__c
                WHERE Start_Date__c != null
                AND End_Date__c != null
        ];
        Set<Integer> availableYears = new Set<Integer>();
        for (Work_Period__c workPeriod : workPeriods) {
            availableYears.add(workPeriod.Start_Date__c.year());
            availableYears.add(workPeriod.End_Date__c.year());
        }
        return new List<Integer>(availableYears);
    }

    @AuraEnabled
    public static List<Date> getOffDays(Integer year) {
        Set<Date> setOffDays = new Set<Date>();
        Date startDate = date.newinstance(year, 1, 1);
        Date endDate = date.newinstance(year, 12, 31);
        for (Integer i = 0; i < startDate.daysBetween(endDate) + 1; i++) {
            setOffDays.add(startDate.addDays(i).toStartOfWeek());
        }
        List<Date> listOffDays = new List<Date>();
        listOffDays.addAll(setOffDays);
        return listOffDays ;
    }

    @AuraEnabled
    public static List<Revenue> getRevenue(Integer year) {
        List<Revenue> revenues = new List<Revenue>();
        List<Work_Period__c> workPeriods = [
                SELECT Start_Date__c, End_Date__c, Order_Custom__r.Account__c,
                        Sales_Rate__r.Type__c, Sales_Rate__r.Sales_Price__c, Sales_Rate__r.Cost_Price__c
                FROM Work_Period__c
                WHERE (CALENDAR_YEAR(Start_Date__c) = :year
                OR CALENDAR_YEAR(End_Date__c) = :year)
                LIMIT 50000
        ];
        revenues = calculateWorkPeriod(workPeriods);
        return calculateWorkPeriod(workPeriods);
    }

    private static List<Revenue> calculateWorkPeriod(List<Work_Period__c> workPeriods) {
        List<Revenue> revenues = new List<Revenue>();
        for (Work_Period__c workPeriod : workPeriods) {
            Integer daysBetween = workPeriod.Start_Date__c.daysBetween(workPeriod.End_Date__c) + 1;
            for (Integer i = 0; i < daysBetween; i++) {
                revenues.add(
                        new Revenue (
                                workPeriod.Start_Date__c.addDays(i).year(),
                                workPeriod.Start_Date__c.addDays(i).toStartOfMonth(),
                                workPeriod.Start_Date__c.addDays(i).toStartOfWeek(),
                                workPeriod.Sales_Rate__r.Sales_Price__c,
                                workPeriod.Sales_Rate__r.Sales_Price__c - workPeriod.Sales_Rate__r.Cost_Price__c,
                                null,
                                null,
                                workPeriod
                        )
                );
            }
        }
        return addAccountsName(revenues);
    }

    private static List<Revenue> addAccountsName(List<Revenue> revenues) {
        List<Id> idsAccount = new List<Id>();
        for (Revenue revenue : revenues) {
            idsAccount.add(revenue.workPeriod.Order_Custom__r.Account__c);
        }
        Map<ID, Account> accountMap = new Map<ID, Account>([
                SELECT Id, Name
                FROM Account
                WHERE Id IN:idsAccount
        ]);
        for (Revenue revenue : revenues) {
            revenue.accountName = accountMap.get(revenue.workPeriod.Order_Custom__r.Account__c).Name;
        }
        return revenues;
    }


    public class Revenue {
        @AuraEnabled
        public Integer year { get; set; }
        @AuraEnabled
        public Date startOfMonth { get; set; }
        @AuraEnabled
        public Date startOfWeek { get; set; }
        @AuraEnabled
        public Decimal revenue { get; set; }
        @AuraEnabled
        public Decimal margin { get; set; }
        @AuraEnabled
        public Decimal total { get; set; }
        @AuraEnabled
        public String accountName { get; set; }
        @AuraEnabled
        public Work_Period__c workPeriod { get; set; }

        Revenue(
                Integer year,
                Date startOfMonth,
                Date startOfWeek,
                Decimal revenue,
                Decimal margin,
                Decimal total,
                String accountName,
                Work_Period__c workPeriod
        ) {
            this.year = year;
            this.startOfMonth = startOfMonth;
            this.startOfWeek = startOfWeek;
            this.revenue = revenue;
            this.margin = margin;
            this.total = total;
            this.accountName = accountName;
            this.workPeriod = workPeriod;
        }
    }

}