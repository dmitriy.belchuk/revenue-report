import { LightningElement, track, api, wire } from 'lwc';
export default class RevenueReportRow extends LightningElement {
    @api
    set value(value) {
        this.currValue = [...value];
        this.handleGroupBy('startOfMonth');
    }
    get value() {
        return this.currValue;
    }

    @api
    set accountName(value) {
        this.curAccountName = value;
    }
    get accountName() {
        return this.curAccountName;
    }

    @api
    handleGroupBy(event) {
        let data;
        if (event === 'startOfMonth') {
            data = this.groupBy(this.currValue, item => item.startOfMonth);
            this.isMonth = true;
        } else {
            data = this.groupBy(this.currValue, item => item.startOfWeek);
            this.isMonth = false;
        }
        this.calc(data);
    }

    @api offDays

    @track dataTable = [];
    @track totalMargin;
    @track totalRevenue;
    @track isMonth = true;

    offDays;
    currValue = [];
    curAccountName = '';
    startOfMonths = [''];

    calc(data) {
        this.dataTable = [];
        for (var [key, value] of data) {
            var mydate = new Date(key.toString());
            this.dataTable.push(
                {
                    key: key,
                    revenue: value.reduce((a, b) => a + b.revenue, 0),
                    margin: value.reduce((a, b) => a + b.margin, 0),
                    month: mydate.getMonth()
                }
            );
        }
        this.totalMargin = this.dataTable.reduce((a, b) => a + b.margin, 0);
        this.totalRevenue = this.dataTable.reduce((a, b) => a + b.revenue, 0);
        if (this.isMonth) {
            this.addMissingMonths(this.dataTable);
        } else {
            this.addMissingOffDays(this.dataTable);
        }
    }

    addMissingMonths(dummyData) {
        var monthsOfWeek = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];
        var existing = [];
        for (var i = 0; i < dummyData.length; i++) {
            existing[i] = dummyData[i]['month'];
        }
        for (var i = 0; i < monthsOfWeek.length; i++) {
            if (existing.indexOf(parseInt(monthsOfWeek[i])) < 0) {
                var dummyObject = {
                    "month": i,
                    "revenue": 0,
                    "margin": 0
                };
                dummyData.push(dummyObject);
            }
        }
        dummyData.sort(function (x, y) { return parseInt(x.month) - parseInt(y.month) });
    }

    addMissingOffDays(dummyData) {
        var year = new Date(dummyData[0].key.toString()).getFullYear();
        var existing = [];
        for (var i = 0; i < dummyData.length; i++) {
            existing[i] = dummyData[i]['key'];
        }
        for (var i = 0; i < this.offDays.length; i++) {
            if (!existing.includes(this.offDays[i])) {
                var dummyObject = {
                    "key": this.offDays[i],
                    "revenue": 0,
                    "margin": 0
                };
                dummyData.push(dummyObject);
            }
        }
        dummyData.sort(function (x, y) { return new Date(x.key) - new Date(y.key) });
    }

    groupBy(list, keyGetter) {
        const map = new Map();
        list.forEach((item) => {
            const key = keyGetter(item);
            const collection = map.get(key);
            if (!collection) {
                map.set(key, [item]);
            } else {
                collection.push(item);
            }
        });
        return map;
    }
}