import { LightningElement, api, track, wire } from 'lwc';
import getAvailableYears from "@salesforce/apex/RevenueReportController.getAvailableYears";
import getRevenue from "@salesforce/apex/RevenueReportController.getRevenue";
import getOffDays from "@salesforce/apex/RevenueReportController.getOffDays";
export default class RevenueReport extends LightningElement {

    @track years = [];
    @track offDays;
    @track selectedYear = '';
    @track selectedGroupBy;
    @track isMonth = true;
    @track mapData = [];
    @track months = [
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December"
    ];

    @wire(getAvailableYears)
    wiredYears({ error, data }) {
        if (data) {
            for (var i = 0; i < data.length; i++) {
                this.years = [...this.years, { value: "" + data[i], label: "" + data[i] }];
            }
            this.error = undefined;
        } else if (error) {
            this.error = error;
        }
    }

    get yearsOption() {
        return this.years;
    }

    get groupByOption() {
        return [{ label: "month", value: 'startOfMonth' }, { label: "week", value: 'startOfWeek' }];
    }

    makeServerCall(action, params, callback, errorCallback) {
        action(params)
            .then(result => {
                callback(result);
            })
            .catch(errors => {
                if (errorCallback) {
                    errorCallback(errors);
                } else {
                    console.log(errors);
                }
            });
    }

    handleChangeYear(event) {
        let self = this;
        this.selectedYear = event.detail.value;
        this.makeServerCall(
            getOffDays,
            { year: self.selectedYear },
            function (result) {
                self.offDays = result;
            },
            function (error) {
                self.error = error;
            }
        );
        this.makeServerCall(
            getRevenue,
            { year: self.selectedYear },
            function (result) {
                let data = self.groupBy(result, item => item.accountName);
                self.mapData = [];
                for (var [key, value] of data) {
                    self.mapData.push({ "value": value, "key": key });
                }
                self.selectedGroupBy = 'startOfMonth'
                self.isMonth = true;
            },
            function (error) {
                self.error = error;
            }
        );
    }
    
    handleChangeGroupBy(event) {
        this.selectedGroupBy = event.detail.value;
        if (this.selectedGroupBy === 'startOfMonth') {
            this.isMonth = true;
        } else {
            this.isMonth = false;
        }
        this.changeGroupBy();
    }

    changeGroupBy() {
        let rowComponents = this.template.querySelectorAll(
            "c-revenue-report-row"
        );
        for (let i = 0; i < rowComponents.length; i++) {
            rowComponents[i].handleGroupBy(this.selectedGroupBy);
        }
    }

    groupBy(list, keyGetter) {
        const map = new Map();
        list.forEach((item) => {
            const key = keyGetter(item);
            const collection = map.get(key);
            if (!collection) {
                map.set(key, [item]);
            } else {
                collection.push(item);
            }
        });
        return map;
    }
}